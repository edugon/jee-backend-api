import { Component, Injectable, OnInit } from '@angular/core';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

	hide: boolean = true;
	message: string;
	color: string;

  	ngOnInit(): void { }

  	showAlert(type: string, message: string) {
		if (type === 'failure')
			this.color = 'alert-danger';
		else
			this.color = 'alert-success';

		this.message = message;
		this.hide = false;

		setTimeout(() => {
			this.hide = true;
		}, 4000);
	}
	 
	dismissAlert() {
		this.hide = true;
	}
}
