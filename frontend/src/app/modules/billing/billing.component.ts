import { Component, Input, OnInit } from '@angular/core';
import { BillingService } from '../../services/billing.service';
import { Billing } from '../../models/Billing';
import { AlertComponent } from '../alert/alert.component';

@Component({
	selector: 'app-billing',
	templateUrl: './billing.component.html',
	styleUrls: ['./billing.component.css']
})
export class BillingComponent implements OnInit {

	@Input() billing: Billing;

	constructor(
		private billingService: BillingService,
		private alertComponent: AlertComponent
	) {
		this.billing = new Billing();
	}

	ngOnInit(): void { }

	// add billing information
	addBilling(): void {
		this.billingService.addBilling(this.billing).subscribe(data => {
			this.alertComponent.showAlert('success', 'Product added successfully!');
		}, err => {
            if (err.error['message'])
                this.alertComponent.showAlert('failure', err.error['message']);
            else
                this.alertComponent.showAlert('failure', 'Something went wrong! D:');
        });;
	}
}