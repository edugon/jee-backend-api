import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertComponent } from '../alert/alert.component'
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/User';
import { HttpClientService } from 'src/app/services/http.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  	selector: 'app-login',
  	templateUrl: './login.component.html',
  	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  	showSignup: boolean;
  	showLogin: boolean;
	salutationEnum: string[];
	@Input() user: User;

  	constructor(
		private api: HttpClientService,
		private router: Router,
		private userService: UserService,
		private orderService: OrderService,
		private alertComponent: AlertComponent
	) {
		this.user = new User();
	}

  	ngOnInit(): void {
    	this.salutationEnum = ['Choose salutation', 'Mr', 'Ms', 'Mrs'];
    	this.showLogin = true;
    	this.showSignup = false;
  	}

  	// hide and show signup/login
  	changeContainer(container: string): void {
    	if (container === 'signup') {
      		this.showLogin = false;
      		this.showSignup = true;
    	} else if (container === 'login') {
      		this.showSignup = false;
      		this.showLogin = true;
    	}
  	}

  	// get value from salutation combobox
  	changeSalutation(salutation: string): void {
    	this.user.setSalutation(salutation);
  	}

  	// perform login
  	doLogin(): void {
    	let credentials = {
      		username: this.user.getUserName(),
      		password: this.user.getPassword()
    	};
		this.userService.login(credentials)
			.subscribe(data => {
				this.user.copy(data);
				this.userService.setLoggedUser(this.user);
				this.api.setToken(this.user.getToken());
				this.orderService.initOrder();
				this.router.navigateByUrl('/catalog');
      		}, err => {
				if (err.error['message'])
					this.alertComponent.showAlert('failure', err.error['message']);
				else
					this.alertComponent.showAlert('failure', 'Something went wrong! D:');
			});
  }

  	// perform signup
  	doSignup(): void {
		this.userService.signUp(this.user)
			.subscribe(data => {
				this.user.copy([data]);
				this.changeContainer('login');
			}, err => {
				if (err.error['message'])
					this.alertComponent.showAlert('failure', err.error['message']);
				else
					this.alertComponent.showAlert('failure', 'Something went wrong! D:');
			});
  	}
}