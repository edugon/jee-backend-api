import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

    item: string;

    constructor(private userService: UserService) { }

    ngOnInit(): void { }

    doSignOut(): void {
        this.userService.signOut();
    }
}
