import { Component, Injectable, OnInit } from '@angular/core';
import { AlertComponent } from '../alert/alert.component';
import { OrderService } from 'src/app/services/order.service';
import { Product } from 'src/app/models/Product';
import { Order } from 'src/app/models/Order';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

	products: Product[];

	constructor(
		private router: Router,
		private orderService: OrderService,
		private alertComponent: AlertComponent
	) {
		this.products = [];
	}

	ngOnInit(): void {
		this.listCart();
	}

	// list shopping card items
	listCart(): void {
		this.products = this.orderService.getOrder().getProducts();
	}

	// get final price
  	getTotal(): string {
		let total = 0;
		for(let i = 0; i < this.products.length; i++)
			total += this.products[i]['price'];
    	return total.toFixed(2);
  	}

	// purchase items from shopping card
	doPurchase(): void {
		this.orderService.payOrder().subscribe(
			data => {
				this.alertComponent.showAlert('success', 'Products purchased!');
				// reset order
				this.orderService.getOrder().setId(null);
				this.orderService.getOrder().setProducts([]);
				this.router.navigateByUrl('/catalog');
			}, err => {
				if (err.error['message'])
                	this.alertComponent.showAlert('failure', err.error['message']);
            	else
                	this.alertComponent.showAlert('failure', 'Something went wrong! D:');
			});
	}
}