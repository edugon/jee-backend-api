import { Component, Input, OnInit } from '@angular/core';
import { AlertComponent } from '../alert/alert.component';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/models/Product';
import { OrderService } from 'src/app/services/order.service';

@Component({
  	selector: 'app-catalog',
  	templateUrl: './catalog.component.html',
  	styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {

	products: Object;

  	constructor(
		private productService: ProductService,
		private OrderService: OrderService,
		private alertComponent: AlertComponent,
	) { }

	ngOnInit(): void {
		this.listCatalog();
	}

	// list available products
	listCatalog(): void {
		this.productService.getCatalog()
			.subscribe(data => {
				this.products = data;
			});
	}

	// add product from catalog to card
	addToCart(product: Product) {
		if (product.stock > 0) {
			this.productService.purchaseProduct(product)
				.subscribe(data => {
					this.alertComponent.showAlert('success', 'Product added successfully!');
					this.OrderService.getOrder().setId(data['id']);
				}, err => {
					if (err.error['message'])
                		this.alertComponent.showAlert('failure', err.error['message']);
            		else
                		this.alertComponent.showAlert('failure', 'Something went wrong! D:');
				});
		} else {
			this.alertComponent.showAlert('failure', 'Product is out of stock :(');
		}
	}
}
