import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class HttpClientService {

	token: string;

	constructor(private http: HttpClient) { }

	setToken(token: string): void  {
		this.token = token;
	}

	createAuthorizationHeader(headers: HttpHeaders): HttpHeaders  {
		if (this.token) {
			return headers.append('Authorization', this.token);
		}
	}

	get(url: string): Observable<Object> {
		let headers = new HttpHeaders();
		headers = this.createAuthorizationHeader(headers);
		return this.http.get(url, { headers: headers });
	}

	post(url: string, data): Observable<Object>  {
		let headers = new HttpHeaders();
		headers = this.createAuthorizationHeader(headers);
		return this.http.post(url, data, { headers: headers });
	}

	put(url: string, data): Observable<Object>  {
		let headers = new HttpHeaders();
		headers = this.createAuthorizationHeader(headers);
		return this.http.put(url, data, { headers: headers });
	}
}