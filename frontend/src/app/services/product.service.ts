import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConfig } from '../config/config';
import { Product } from '../models/Product';
import { HttpClientService } from './http.service';
import { OrderService } from './order.service';

@Injectable({ providedIn: 'root' })
export class ProductService {

    constructor(
        private api: HttpClientService,
        private orderService: OrderService
    ) { }

    getCatalog(): Observable<Object> {
        return this.api.get(AppConfig.productsUrl);
    }

    purchaseProduct(product: Product): Observable<Object> {
        return this.orderService.addProduct(product);
    }
}