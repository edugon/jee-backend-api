import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConfig } from '../config/config';
import { Order } from '../models/Order';
import { Product } from '../models/Product';
import { HttpClientService } from './http.service';
import { UserService } from './user.service';

@Injectable({ providedIn: 'root' })
export class OrderService {

    private order: Order;

    constructor(
        private api: HttpClientService,
        private userService: UserService) {
        this.order = new Order();
        this.order.setProducts([]);
    }

    // init order when login
    initOrder(): void {
        this.findOrder()
            .subscribe(data => {
                if (data[0]) {
                    this.order.setId(data[0]['id']);
                    this.order.setProducts(data[0]['products']);
                    this.order.setOwner(data[0]['owner']);
                    this.order.setStatus(data[0]['status']);
                    console.log(this.order);
                }
            });
    }

    // create order
    createOrder(): Observable<Object> {
        return this.api.post(AppConfig.ordersUrl, this.order);
    }

    // set local order
    setOrder(order: Order): void {
        this.order = order;
    }

    // get local order
    getOrder(): Order {
        return this.order;
    }

    // find order by user
    findOrder(): Observable<Object> {
        return this.api.get(AppConfig.ordersUrl
            + '?owner.id=' + this.userService.getLoggedUser().getId()
            + '&status=PENDING');
    }

    // add product to order
    addProduct(product: Product): Observable<Object> {
        this.order.setOwner(this.userService.getLoggedUser())
        this.order.getProducts().push(product);
        // check order already exists
        if (this.order.getId()) {
            return this.api.put(AppConfig.ordersUrl, this.order);
        } else {
            this.order.setOwner(this.order.getOwner());
            this.order.setStatus('PENDING');
            return this.createOrder();
        }
    }   

    // update order to in progress
    payOrder(): Observable<Object> {
        this.order.setOwner(this.userService.getLoggedUser());
        this.order.setStatus('IN_PROGRESS');
        return this.api.put(AppConfig.ordersUrl, this.order);
    }
}