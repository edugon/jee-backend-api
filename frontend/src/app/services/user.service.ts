import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConfig } from '../config/config';
import { User } from '../models/User';
import { HttpClientService } from './http.service';
import { OrderService } from './order.service';


@Injectable({ providedIn: 'root' })
export class UserService {

    private user: User;

    constructor(
        private api: HttpClientService) {
        this.user = new User();
    }

    setLoggedUser(user: User): void {
        this.user = user;
    }

    getLoggedUser(): User {
        return this.user;
    }

    login(credentials: Object): Observable<Object> {
        return this.api.post(AppConfig.authUrl + '/login', credentials);
    }

    signUp(user: User): Observable<Object> {
        return this.api.post(AppConfig.authUrl + '/signup', user);
    }

    signOut(): void {
        location.reload();
    }
}