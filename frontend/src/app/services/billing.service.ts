import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConfig } from '../config/config';
import { Billing } from '../models/Billing';
import { HttpClientService } from './http.service';
import { UserService } from './user.service';

@Injectable({ providedIn: 'root' })
export class BillingService {

    constructor(
        private api: HttpClientService,
        private userService: UserService
    ) { }
    
    addBilling(billing: Billing): Observable<Object> {
        billing.setOwner(this.userService.getLoggedUser());
        return this.api.post(AppConfig.billingUrl, billing);
    }
}