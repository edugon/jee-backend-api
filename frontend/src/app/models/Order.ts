import { Billing } from './Billing';
import { User } from './User';
import { Product } from './Product';

export class Order {

    id: number;
    owner: User;
    billing: Billing;
    products: Product[];
    status:	string;

    constructor() { }

    public getId(): number {
        return this.id;
    }

    public setId(id: number): void {
        this.id = id;
    }

    public getOwner(): User {
        return this.owner;
    }

    public setOwner(owner: User): void {
        this.owner = owner;
    }

    public getBilling(): Billing {
        return this.billing;
    }

    public setBilling(billing: Billing): void {
        this.billing = billing;
    }

    public getProducts(): Product[] {
        return this.products;
    }

    public setProducts(products: Product[]): void {
        this.products = products;
    }

    public getStatus(): string {
        return this.status;
    }

    public setStatus(status: string): void {
        this.status = status;
    }
}