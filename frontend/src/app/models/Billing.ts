import { User } from './User';

export class Billing {

    owner: User;
    firstName: string;
    lastName: string;
    country: string;
    locality: string;
    address: string;
    postalCode: number;

    constructor() { }

    public getOwner(): User {
        return this.owner;
    }

    public setOwner(owner: User): void {
        this.owner = owner;
    }

    public getFirstName(): string {
        return this.firstName;
    }

    public setFirstName(firstName: string): void {
        this.firstName = firstName;
    }

    public getLastName(): string {
        return this.lastName;
    }

    public setLastName(lastName: string): void {
        this.lastName = lastName;
    }

    public getCountry(): string {
        return this.country;
    }

    public setCountry(country: string): void {
        this.country = country;
    }

    public getLocality(): string {
        return this.locality;
    }

    public setLocality(locality: string): void {
        this.locality = locality;
    }

    public getAddress(): string {
        return this.address;
    }

    public setAddress(address: string): void {
        this.address = address;
    }

    public getPostalCode(): number {
        return this.postalCode;
    }

    public setPostalCode(postalCode: number): void {
        this.postalCode = postalCode;
    }
}