export class User {

    id: number;
    salutation: string;
    email: string;
    firstName: string;
    lastName: string;
    username: string;
    password: string;
    role: string;
    token: string;

    copy(data: Object) {
        this.id = data['id'];
        this.salutation = data['salutation'];
        this.email = data['email'];
        this.firstName = data['firstName'];
        this.lastName = data['lastName'];;
        this.username = data['username'];;
        this.password = data['password'];;
        this.role = data['role'];;
        this.token = data['token'];;
    }

    public getId(): number {
        return this.id;
    }

    public setId(id: number): void {
        this.id = id;
    }

    public getSalutation(): string {
        return this.salutation;
    }

    public setSalutation(salutation: string): void {
        this.salutation = salutation;
    }

    public getEmail(): string {
        return this.email;
    }

    public setEmail(email: string): void {
        this.email = email;
    }

    public getFirstName(): string {
        return this.firstName;
    }

    public setFirstName(firstName: string): void {
        this.firstName = firstName;
    }

    public getLastName(): string {
        return this.lastName;
    }

    public setLastName(lastName: string): void {
        this.lastName = lastName;
    }

    public getUserName(): string {
        return this.username;
    }

    public setUserName(username: string): void {
        this.username = username;
    }

    public getPassword(): string {
        return this.password;
    }

    public setPassword(password: string): void {
        this.password = password;
    }

    public getRole(): string {
        return this.role;
    }

    public setRole(role: string): void {
        this.role = role;
    }

    public getToken(): string {
        return this.token;
    }

    public setToken(token: string): void {
        this.token = token;
    }
}