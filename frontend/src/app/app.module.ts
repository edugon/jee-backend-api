import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from "@angular/common";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './modules/login/login.component';
import { NavbarComponent } from './modules/navbar/navbar.component';
import { CatalogComponent } from './modules/catalog/catalog.component';
import { CartComponent } from './modules/cart/cart.component';
import { BillingComponent } from './modules/billing/billing.component';
import { OrdersComponent } from './modules/orders/orders.component';
import { FooterComponent } from './modules/footer/footer.component';
import { AlertComponent } from './modules/alert/alert.component';

@NgModule({
  	declarations: [
		AppComponent,
		LoginComponent,
		NavbarComponent,
		CatalogComponent,
		CartComponent,
		BillingComponent,
		OrdersComponent,
		FooterComponent,
		AlertComponent
  	],
  	imports: [
		CommonModule,
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		HttpClientModule
  	],
  	providers: [],
  	bootstrap: [AppComponent]
})
export class AppModule { }
