import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BillingComponent } from './modules/billing/billing.component';
import { CartComponent } from './modules/cart/cart.component';
import { CatalogComponent } from './modules/catalog/catalog.component';
import { FooterComponent } from './modules/footer/footer.component';
import { LoginComponent } from './modules/login/login.component';
import { NavbarComponent } from './modules/navbar/navbar.component';
import { OrdersComponent } from './modules/orders/orders.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'catalog', component: CatalogComponent },
  { path: 'cart', component: CartComponent },
  { path: 'billing', component: BillingComponent },
  { path: 'orders', component: OrdersComponent },
  { path: 'navbar', component: NavbarComponent },
  { path: 'footer', component: FooterComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
