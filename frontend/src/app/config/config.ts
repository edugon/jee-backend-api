export class AppConfig {

    public static billingUrl: string = 'http://localhost:8080/api/billing';
    public static ordersUrl: string = 'http://localhost:8080/api/orders';
    public static productsUrl: string = 'http://localhost:8080/api/products';
    public static authUrl: string = 'http://localhost:8080/api/auth';

}