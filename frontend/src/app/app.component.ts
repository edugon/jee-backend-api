import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { UserService } from './services/user.service';

@Component({
  	selector: 'app-root',
  	templateUrl: './app.component.html',
  	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  	constructor(
		private router: Router,
		private userService: UserService
	) { }

  	ngOnInit(): void {
		if (this.userService.getLoggedUser().getToken()) {
			this.router.navigateByUrl('/catalog');
		} else {
			this.router.navigateByUrl('/login');
		}
  	}
}
