
# jee-backend-api
This document describes the deployment, functioning and testing of this ecommerce application.
## Directory listing
1. **`backend/`** -> Sources and configuration folder for backend.
2. **`database/`** -> Sources and configuration folder for database.
3. **`frontend/`** -> Sources and configuration folder for frontend.
## About
Small ecommerce application. This project allows customers to authenticate within the system and take a look into a set of products in order to perform a purchase. It also manages the creation of billing information associated to orders as well as creating new customers via signup.
## Prerequisites
- **Unix based distribution** (tested with [Debian 10](https://cdimage.debian.org/debian-cd)).
- **[Docker Engine](https://docs.docker.com/engine/)** and **[Docker Compose](https://docs.docker.com/compose/)** (tested with v17.05.0-ce and v1.24.1).
- **[Git](https://git-scm.com/downloads)** version-control system.
> **Note**: In case you want to deploy using **Docker Toolbox** (absolutely deprecated), please consider upgrading to **Docker for Windows/Mac** or change IP pointers from `localhost` to your **Docker Toolbox VM** (`$ docker-machine ip`).
## Deployment
1. Clone this respository into your workspace: ``$ git clone https://gitlab.com/edugon/jee-backend-api.git``.
4. Run docker containers: ``$ docker-compose up``.
5. Once everything is up and running, the system will be listening at ``http://localhost:4200``.
6. Enjoy! :)
## Testing
First of all, open a web browser and go to: ``http://localhost:4200``, there you should see a login screen. If you want, you can access using following testing (admin) account: ``username: batman / password: batman``. Do not hesitate to singup and create your own account!
> **Note:** Mysql database is populated at container runtime with some sample data.

In addition, some integration tests are fired at image build time. Check logs!
## Resources naming
This system is **swagger powered!** This means that you just have to go to ``http://localhost:8080/api/swagger-ui.html`` and you will be able to check all the endpoints! :)
