package org.apache.maven.repository;

import org.apache.maven.entity.Order;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface OrderRepository extends PagingAndSortingRepository<Order, Long>, QueryByExampleExecutor<Order> {

}
