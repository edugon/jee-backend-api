package org.apache.maven.repository;

import org.apache.maven.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface UserRepository extends PagingAndSortingRepository<User, Long>, QueryByExampleExecutor<User> {

}
