package org.apache.maven.repository;

import org.apache.maven.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface ProductRepository extends PagingAndSortingRepository<Product, Long>, QueryByExampleExecutor<Product> {
	
}
