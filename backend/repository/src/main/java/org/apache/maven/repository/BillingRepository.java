package org.apache.maven.repository;

import org.apache.maven.entity.Billing;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface BillingRepository extends PagingAndSortingRepository<Billing, Long>, QueryByExampleExecutor<Billing> {

}
