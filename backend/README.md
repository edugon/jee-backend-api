# Backend
This document describes the deployment, functioning and testing of this backend.
## Directory listing
1. **`controller/`** -> Sources folder for controller module.
2. **`model/`** -> Sources folder for model module.
3. **`repository/`** -> Sources folder for repository module.
4. **`security/`** -> Sources folder for security module.
5. **`service/`** -> Sources folder for service module.
6. **`test/`** -> Sources folder for test module.
7. **`utils/`** -> Sources folder for utils module.
8. **`webapp/`** -> Sources folder for webapp module.
## About
Small ecommerce application. This project allows customers to authenticate within the system and take a look into a set of products in order to perform a purchase. It also manages the creation of billing information associated to orders as well as creating new customers via signup. This folder contains backend server.
> The API url is ``http://localhost:8080/api/*``
## Deployment
Just run docker container with ``docker-compose up backend``.
## Authentication
Backend server uses Spring Security for security module. JSON Web Token (JWT) is the main authentication method. Performing a ``/login`` with valid credentials will return a JWT ``Bearer token``.  Just add this token in your headers when performing requests to the server ``Authorization: Bearer xyz``.
> **Desired limitation:** Backend server does not support token refreshing system. You have 2 minutes before token expires, once this happen you will have to ``/login`` again into the system.
## Authorization
Endpoints are secured using role system:
- ``/auth/`` domain is an anonymous domain, it allows users to perform ``/signup``, ``/login`` and ``/logout``
- ``/users/*`` domain is authorized with ``ROLE_ADMIN`` privileges. 
- The rest of endpoints will require ``ROLE_USER`` privileges.
## Testing
Some integration tests are fired at image build time. Check logs! Service layer is partially tested just as example.
## Resources naming
This system is **swagger powered!** This means that you just have to go to ``http://localhost:8080/api/swagger-ui.html`` and you will be able to check all the endpoints! :)