package org.apache.maven.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.maven.service.UserService;
import org.apache.maven.service.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@RestController
@RequestMapping("users")
public class UserController {
	
	private final String FIELD_PAGE = "page";
	private final String FIELD_SIZE = "size";
	private final String DEFAULT_PAGE = "0";
	private final String DEFAULT_SIZE = "10";
	
	@Autowired
	UserService userService;
	
	/**
	 * Get a list of User items.
	 * @param page Value of page index.
	 * @param size Value of page size.
	 * @param userCriteria Filtering criteria parameters.
	 * @return The list of User items.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@GetMapping(produces = "application/json")
	public List<UserDto> getUsers(
			@RequestParam(value = FIELD_PAGE, defaultValue = DEFAULT_PAGE, required = false) Integer page,
			@RequestParam(value = FIELD_SIZE, defaultValue = DEFAULT_SIZE, required = false) Integer size,
			@Parameter(hidden = true) UserDto userCriteria) {
		return userService.listUsers(page, size, userCriteria);
	}
	
	/**
	 * Get a specific User item.
	 * @param userId Identifier of User item.
	 * @return The specified User item.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@GetMapping(produces = "application/json", value = "/{id}")
	public UserDto getUser(@PathVariable(name = "id") Long userId) {
		UserDto userCriteria = new UserDto();
		userCriteria.setId(userId);
		return userService.findUser(userCriteria);
	}
	
	/**
	 * Create a new User item.
	 * @param newUser Data of new User item.
	 * @return The created User item.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@PostMapping(consumes = "application/json", produces = "application/json")
	public UserDto createUser(@Valid @RequestBody(required = true) UserDto newUser) {
		return userService.createUser(newUser);
	}
	
	/**
	 * Update an existing User item.
	 * @param updatedUser User item with data to be updated.
	 * @return The updated User item.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@PutMapping(consumes = "application/json", produces = "application/json")
	public UserDto updateUser(@RequestBody(required = true) UserDto updatedUser) {
		return userService.updateUser(updatedUser);
	}
	
	/**
	 * Delete an existing User item.
	 * @param userId Identifier of User item.
	 * @return Success message.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@DeleteMapping(produces = "application/json", value = "/{id}")
	public String deleteUser(@PathVariable(name = "id") Long userId) {
		userService.deleteUser(userId);
		return "\"User " + userId + " deleted successfully\"";
	}
}
