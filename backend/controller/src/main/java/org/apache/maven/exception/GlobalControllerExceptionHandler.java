package org.apache.maven.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Set;
import java.util.logging.Logger;

import javax.validation.ConstraintViolation;

import org.apache.maven.service.exception.CustomException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Exception handler, in charge of managing errors thrown from lower layers.
 */
@ControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler {
	
	private final static Logger LOGGER = Logger.getLogger(GlobalControllerExceptionHandler.class.getName());
	
	private final String FIELD_MESSAGE = "message";
	private final String FIELD_CONSTRAINTS = "constraints";
	private final String MESSAGE_CONSTRAINTS = "Some constraints were not satisfied!";
	private final String MESSAGE_DUPLICATED_ITEM = "Item already exists";
	
	private ObjectMapper mapper;
	
	/**
	 * Handle custom exceptions thrown by service module.
	 * @param exception CustomException that is thrown.
	 * @return JSON formatted message
	 * @throws JsonProcessingException Problems processing JSON response
	 */
	@ExceptionHandler(CustomException.class)
	public ResponseEntity<String> handleCustom(CustomException exception)
			throws JsonProcessingException {
		LOGGER.severe(exception.getMessage());
		// create and format JSON response
		mapper = new ObjectMapper();
		ObjectNode body = mapper.createObjectNode();
		body.put(FIELD_MESSAGE, exception.getMessage());
		String bodyString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(body);
		return new ResponseEntity<String>(bodyString, HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * Handle constraint violations thrown by persistence module.
	 * Main purpose: check unique key validations.
	 * @param exception Constraint violation that is thrown.
	 * @return JSON formatted message
	 * @throws JsonProcessingException Problems processing JSON response
	 */
	@ExceptionHandler(SQLIntegrityConstraintViolationException.class)
	public ResponseEntity<String> handleDuplicatedKey(SQLIntegrityConstraintViolationException exception)
			throws JsonProcessingException {
		LOGGER.severe(exception.getMessage());
		// create and format JSON response
		mapper = new ObjectMapper();
		ObjectNode body = mapper.createObjectNode();
		body.put(FIELD_MESSAGE, exception.getMessage());
		String bodyString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(body);
		return new ResponseEntity<String>(bodyString, HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * Handle constraint violations thrown by Java/Hibernate validation.
	 * @param exception Constraint violation/s that are thrown.
	 * @return JSON formatted message
	 * @throws JsonProcessingException Problems processing JSON response
	 */
    @ExceptionHandler({
    	javax.validation.ConstraintViolationException.class, 
    	org.hibernate.exception.ConstraintViolationException.class
    })
    public ResponseEntity<String> handleConstraintViolation(RuntimeException exception)
    		throws JsonProcessingException {
    	LOGGER.severe(exception.getMessage());
    	Throwable cause = exception.getCause(); // get exception cause to check instance
    	mapper = new ObjectMapper();
    	String bodyString = "";
    	ArrayNode constraints = null;
    	// if hibernate constraint, we receive just a message
    	if (cause instanceof org.hibernate.exception.ConstraintViolationException) {
    		ObjectNode body = mapper.createObjectNode();
    		// create and format JSON response
    		constraints = mapper.createArrayNode();
    		constraints.add(MESSAGE_DUPLICATED_ITEM);
    		body.put(FIELD_MESSAGE, MESSAGE_CONSTRAINTS);
    		body.set(FIELD_CONSTRAINTS, constraints);
    		bodyString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(body);
    	// if javax constraint, we receive a set of messages
    	} else {
    		Set<ConstraintViolation<?>> constraintViolations = 
    				((javax.validation.ConstraintViolationException) exception).getConstraintViolations();
    		// create and format JSON response
    		constraints = mapper.createArrayNode();
    		for (ConstraintViolation<?> constraintViolation : constraintViolations) {
    			// populate constraints array
    			String property = constraintViolation.getPropertyPath().toString();
    			String message = constraintViolation.getMessage().toString();
    			constraints.add(property + " " + message);
    		}
    		ObjectNode body = mapper.createObjectNode();
    		// add constraints and message to body
    		body.put(FIELD_MESSAGE, MESSAGE_CONSTRAINTS);
    		body.set(FIELD_CONSTRAINTS, constraints);
    		bodyString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(body);
    	}
		return new ResponseEntity<String>(bodyString, HttpStatus.BAD_REQUEST);
    }
}
