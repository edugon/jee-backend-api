package org.apache.maven.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.maven.service.OrderService;
import org.apache.maven.service.dto.OrderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@RestController
@RequestMapping("orders")
public class OrderController {
	
	private final String FIELD_PAGE = "page";
	private final String FIELD_SIZE = "size";
	private final String DEFAULT_PAGE = "0";
	private final String DEFAULT_SIZE = "10";

	@Autowired
	OrderService orderService;
	
	/**
	 * Get a list of Order items.
	 * @param page Value of page index.
	 * @param size Value of page size.
	 * @param orderCriteria Filtering criteria parameters.
	 * @return The list of Order items.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@GetMapping(produces = "application/json")
	public List<OrderDto> getOrders(
			@RequestParam(value = FIELD_PAGE, defaultValue = DEFAULT_PAGE, required = false) Integer page,
			@RequestParam(value = FIELD_SIZE, defaultValue = DEFAULT_SIZE, required = false) Integer size,
			@Parameter(hidden = true) OrderDto orderCriteria) {
		return orderService.listOrders(page, size, orderCriteria);
	}
	
	/**
	 * Get a specific Billing item.
	 * @param orderId Identifier of Order item.
	 * @return The specified Order item.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@GetMapping(produces = "application/json", value = "/{id}")
	public OrderDto getOrder(@PathVariable(name = "id") Long orderId) {
		return orderService.findOrderById(orderId);
	}
	
	/**
	 * Create a new Order item.
	 * @param newOrder Data of new Order item.
	 * @return The created Order item.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@PostMapping(consumes = "application/json", produces = "application/json")
	public OrderDto createOrder(@Valid @RequestBody OrderDto newOrder) {
		return orderService.createOrder(newOrder);
	}
	
	/**
	 * Update an existing Order item.
	 * @param updatedOrder Order item with data to be updated.
	 * @return The updated Order item.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@PutMapping(consumes = "application/json", produces = "application/json")
	public OrderDto updateOrder(@RequestBody(required = true) OrderDto updatedOrder) {
		return orderService.updateOrder(updatedOrder);
	}
	
	/**
	 * Delete an existing Order item.
	 * @param orderId Identifier of Order item.
	 * @return Success message.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@DeleteMapping(produces = "application/json", value = "/{id}")
	public String deleteOrder(@PathVariable(name = "id") Long orderId) {
		orderService.deleteOrder(orderId);
		return "\"Order " + orderId + " deleted successfully\"";
	}
}
