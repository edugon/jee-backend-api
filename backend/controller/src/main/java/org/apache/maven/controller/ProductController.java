package org.apache.maven.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.maven.service.ProductService;
import org.apache.maven.service.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@RestController
@RequestMapping("products")
public class ProductController {
	
	private final String FIELD_PAGE = "page";
	private final String FIELD_SIZE = "size";
	private final String DEFAULT_PAGE = "0";
	private final String DEFAULT_SIZE = "10";
	
	@Autowired
	ProductService productService;
	
	/**
	 * Get a list of Product items.
	 * @param page Value of page index.
	 * @param size Value of page size.
	 * @param productCriteria Filtering criteria parameters.
	 * @return The list of Product items.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@GetMapping(produces = "application/json")
	public List<ProductDto> getProducts(
			@RequestParam(value = FIELD_PAGE, defaultValue = DEFAULT_PAGE, required = false) Integer page,
			@RequestParam(value = FIELD_SIZE, defaultValue = DEFAULT_SIZE, required = false) Integer size,
			@Parameter(hidden = true) ProductDto productCriteria) {
		return productService.listProducts(page, size, productCriteria);
	}
	
	/**
	 * Get a specific Product item.
	 * @param productId Identifier of Product item.
	 * @return The specified Product item.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@GetMapping(produces = "application/json", value = "/{id}")
	public ProductDto getProduct(@PathVariable(name = "id") Long productId) {
		ProductDto productCriteria = new ProductDto();
		productCriteria.setId(productId);
		return productService.findProduct(productCriteria);
	}
	
	/**
	 * Create a new Product item.
	 * @param newProduct Data of new Product item.
	 * @return The created Product item.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ProductDto createProduct(@Valid @RequestBody(required = true) ProductDto newProduct) {
		return productService.createProduct(newProduct);
	}
	
	/**
	 * Update an existing Product item.
	 * @param updatedProduct Product item with data to be updated.
	 * @return The updated Product item.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@PutMapping(consumes = "application/json", produces = "application/json")
	public ProductDto updateProduct(@RequestBody(required = true) ProductDto updatedProduct) {
		return productService.updateProduct(updatedProduct);
	}
	
	/**
	 * Delete an existing Product item.
	 * @param productId Identifier of Product item.
	 * @return Success message.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@DeleteMapping(produces = "application/json", value = "/{id}")
	public String deleteProduct(@PathVariable(name = "id") Long productId) {
		productService.deleteProduct(productId);
		return "\"Product " + productId + " deleted successfully\"";
	}
}
