package org.apache.maven.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.maven.service.BillingService;
import org.apache.maven.service.dto.BillingDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@RestController
@RequestMapping("billing")
public class BillingController {

	private final String FIELD_PAGE = "page";
	private final String FIELD_SIZE = "size";
	private final String DEFAULT_PAGE = "0";
	private final String DEFAULT_SIZE = "10";

	@Autowired
	BillingService billingService;
	
	/**
	 * Get a list of Billing items.
	 * @param page Value of page index.
	 * @param size Value of page size.
	 * @param billingCriteria Filtering criteria parameters.
	 * @return The list of Billing items.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@GetMapping(produces = "application/json")
	public List<BillingDto> getBilling(
			@RequestParam(value = FIELD_PAGE, defaultValue = DEFAULT_PAGE, required = false) Integer page,
			@RequestParam(value = FIELD_SIZE, defaultValue = DEFAULT_SIZE, required = false) Integer size,
			@Parameter(hidden = true) BillingDto billingCriteria) {
		return billingService.listBilling(page, size, billingCriteria);
	}
	
	/**
	 * Get a specific Billing item.
	 * @param billingId Identifier of Billing item.
	 * @return The specified Billing item.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@GetMapping(produces = "application/json", value = "/{id}")
	public BillingDto getProduct(@PathVariable(name = "id") Long billingId) {
		BillingDto billingCriteria = new BillingDto();
		billingCriteria.setId(billingId);
		return billingService.findBilling(billingCriteria);
	}
	
	/**
	 * Create a new Billing item.
	 * @param newBilling Data of new Billing item.
	 * @return The created Billing item.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@PostMapping(consumes = "application/json", produces = "application/json")
	public BillingDto createBilling(@Valid @RequestBody(required = true) BillingDto newBilling) {
		return billingService.createBilling(newBilling);
	}
	
	/**
	 * Update an existing Billing item.
	 * @param updatedBilling Billing item with data to be updated.
	 * @return The updated Billing item.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@PutMapping(consumes = "application/json", produces = "application/json")
	public BillingDto updateBilling(@RequestBody(required = true) BillingDto updatedBilling) {
		return billingService.updateBilling(updatedBilling);
	}
	
	/**
	 * Delete an existing Billing item.
	 * @param billingId Identifier of Billing item.
	 * @return Success message.
	 */
	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@DeleteMapping(produces = "application/json", value = "/{id}")
	public String deleteBilling(@PathVariable(name = "id") Long billingId) {
		billingService.deleteBilling(billingId);
		return "\"Billing " + billingId + " deleted successfully\"";
	}
}
