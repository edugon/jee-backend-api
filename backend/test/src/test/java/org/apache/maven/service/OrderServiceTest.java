package org.apache.maven.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.maven.app.WebappApplication;
import org.apache.maven.service.dto.OrderDto;
import org.apache.maven.service.dto.ProductDto;
import org.apache.maven.service.dto.UserDto;
import org.apache.maven.types.Category;
import org.apache.maven.types.Role;
import org.apache.maven.types.Salutation;
import org.apache.maven.types.Status;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebappApplication.class)
@TestPropertySource(locations="classpath:persistence.properties")
@ContextConfiguration(classes = { 
	ProductService.class,
	UserService.class,
	OrderService.class
})
public class OrderServiceTest {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private OrderService orderService;
	
	private UserDto ownerDto;
	private ProductDto productDto1;
	private ProductDto productDto2;
	private List<ProductDto> productDtoList;
	private OrderDto orderDto;
	
	@Before
	public void setUp() {
		// insert test owner
		ownerDto = new UserDto();
		ownerDto.setSalutation(Salutation.Mr);
		ownerDto.setFirstName("Rick");
		ownerDto.setLastName("Sanchez");
		ownerDto.setEmail("rick@sanchez");
		ownerDto.setUsername("rick");
		ownerDto.setPassword("rick");
		ownerDto.setRole(Role.ROLE_USER);
		
		if (!userService.existUser(ownerDto)) {
			ownerDto = userService.createUser(ownerDto);
		} else {
			ownerDto = userService.findUser(ownerDto);
		}
		
		productDtoList = new ArrayList<ProductDto>();
		
		// insert test product
		productDto1 = new ProductDto();
		productDto1.setName("Coca cola");
		productDto1.setDescription("Super fresh and cold drink.");
		productDto1.setPrice(1.20f);
		productDto1.setCategory(Category.FOOD);
		productDto1.setStock(100);
		
		if (!productService.existProduct(productDto1)) {
			productDto1 = productService.createProduct(productDto1);
		} else {
			productDto1 = productService.findProduct(productDto1);
		}
		productDtoList.add(productDto1);
		
		// insert test product
		productDto2 = new ProductDto();
		productDto2.setName("Seven Up");
		productDto2.setDescription("Another super fresh and cold drink.");
		productDto2.setPrice(1.20f);
		productDto2.setCategory(Category.FOOD);
		productDto2.setStock(100);
				
		if (!productService.existProduct(productDto2)) {
			productDto2 = productService.createProduct(productDto2);
		} else {
			productDto2 = productService.findProduct(productDto2);
		}
		productDtoList.add(productDto2);
		
		// insert test order
		orderDto = new OrderDto();
		orderDto.setOwner(ownerDto);
		orderDto.setProducts(productDtoList);
		orderDto.setStatus(Status.PENDING);
		
		if (!orderService.existOrder(orderDto)) {
			orderDto = orderService.createOrder(orderDto);
		} else {
			orderDto = orderService.findOrder(orderDto);
		}
	}

	@After
	public void tearDown() {
		// delete test order
		if (orderService.existOrderById(orderDto.getId())) {
			orderService.deleteOrder(orderDto.getId());
		}
		
		// delete test product
		if (productService.existProductById(productDto1.getId())) {
			productService.deleteProduct(productDto1.getId());
		}
		
		// delete test product
		if (productService.existProductById(productDto2.getId())) {
			productService.deleteProduct(productDto2.getId());
		}
	}

	@Test
	public void testListOrders() {
		// create order criteria
		OrderDto orderCriteria = new OrderDto();
		orderCriteria.setStatus(Status.PENDING);
		
		// list pending orders
		List<OrderDto> orderRestuls = orderService.listOrders(0, 10, orderCriteria);
		
		// assert getting expected results
		assertEquals(orderRestuls.size(), 1);
	}

	@Test
	public void testFindOrders() {
		// create order criteria
		OrderDto orderCriteria = new OrderDto();
		orderDto.setId(orderDto.getId());
		
		// find first order item
		OrderDto foundOrder = orderService.findOrder(orderCriteria);
		
		// assert order is being obtained
		assertNotNull(foundOrder);
	}
	
	@Test
	public void testCreateOrder() {
		// create new order
		OrderDto newOrder = new OrderDto();
		newOrder.setOwner(ownerDto);
		newOrder.setProducts(productDtoList);
		newOrder.setStatus(Status.PENDING);
		
		// save new order
		OrderDto savedOrder = orderService.createOrder(newOrder);
		
		// assert order is being created
		assertNotNull(savedOrder);
		
		// delete saved order
		orderService.deleteOrder(savedOrder.getId());
	}
	
	@Test
	public void testUpdateOrder() {
		// create modified order
		OrderDto updatedOrder = new OrderDto();
		updatedOrder.setId(orderDto.getId());
		updatedOrder.setOwner(orderDto.getOwner());
		updatedOrder.setProducts(productDtoList);
		// modify status
		updatedOrder.setStatus(Status.IN_PROGRESS);
		
		// update order
		OrderDto savedOrder = orderService.updateOrder(updatedOrder);
		
		// check order is being updated
		assertEquals(savedOrder.getStatus(), Status.IN_PROGRESS);
	}

	@Test
	public void testDeleteOrder() {
		// create new order
		OrderDto newOrder = new OrderDto();
		newOrder.setOwner(ownerDto);
		newOrder.setProducts(productDtoList);
		newOrder.setStatus(Status.PENDING);
		
		// save new order
		OrderDto savedOrder = orderService.createOrder(newOrder);
		
		// delete saved order
		orderService.deleteOrder(savedOrder.getId());
		
		// assert order is being deleted
		assertFalse(orderService.existOrderById(savedOrder.getId()));
	}
}
