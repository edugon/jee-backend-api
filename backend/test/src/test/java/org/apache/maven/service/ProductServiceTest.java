package org.apache.maven.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.apache.maven.app.WebappApplication;
import org.apache.maven.service.dto.ProductDto;
import org.apache.maven.types.Category;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebappApplication.class)
@TestPropertySource(locations="classpath:persistence.properties")
@ContextConfiguration(classes = { ProductService.class })
public class ProductServiceTest {

	@Autowired
	ProductService productService;
	
	private ProductDto productDto1;
	private ProductDto productDto2;

	@Before
	public void setUp() {
		// insert test product
		productDto1 = new ProductDto();
		productDto1.setName("Coca cola");
		productDto1.setDescription("Super fresh and cold drink.");
		productDto1.setPrice(1.20f);
		productDto1.setCategory(Category.FOOD);
		productDto1.setStock(100);
				
		if (!productService.existProduct(productDto1)) {
			productDto1 = productService.createProduct(productDto1);
		} else {
			productDto1 = productService.findProduct(productDto1);
		}
				
		// insert test product
		productDto2 = new ProductDto();
		productDto2.setName("Seven Up");
		productDto2.setDescription("Another super fresh and cold drink.");
		productDto2.setPrice(1.20f);
		productDto2.setCategory(Category.FOOD);
		productDto2.setStock(100);
						
		if (!productService.existProduct(productDto2)) {
			productDto2 = productService.createProduct(productDto2);
		} else {
			productDto2 = productService.findProduct(productDto2);
		}
	}

	@After
	public void tearDown() {
		// delete test product
		if (productService.existProductById(productDto1.getId())) {
			productService.deleteProduct(productDto1.getId());
		}
				
		// delete test product
		if (productService.existProductById(productDto2.getId())) {
			productService.deleteProduct(productDto2.getId());
		}
		
	}

	@Test
	public void testListProducts() {
		// create product criteria
		ProductDto productCriteria = new ProductDto();
		productCriteria.setCategory(Category.FOOD);
		
		// list food products
		List<ProductDto> productResults = productService.listProducts(0, 10, productCriteria);
		
		// assert getting expected results
		assertEquals(productResults.size(), 2);
	}

	@Test
	public void testFindProduct() {
		// create product criteria
		ProductDto productCriteria = new ProductDto();
		productCriteria.setId(productDto1.getId());
		
		// find first product
		ProductDto foundProduct = productService.findProduct(productCriteria);
		
		// assert product is being obtained
		assertNotNull(foundProduct);
	}

	@Test
	public void testCreateProduct() {
		// create new product
		ProductDto newProduct = new ProductDto();
		newProduct.setName("Nuka cola");
		newProduct.setDescription("Super fresh and cold drink.");
		newProduct.setPrice(1.20f);
		newProduct.setCategory(Category.FOOD);
		newProduct.setStock(100);
		
		// save new product
		ProductDto savedProduct = productService.createProduct(newProduct);
		
		// assert product is being created
		assertNotNull(savedProduct);
		
		// delete saved product
		productService.deleteProduct(savedProduct.getId());
	}
	
	@Test
	public void testUpdateProduct() {
		// create modified product
		ProductDto updatedProduct = new ProductDto();
		updatedProduct.setId(productDto1.getId());
		updatedProduct.setName("Updated name");
		
		// update product
		ProductDto savedProduct = productService.updateProduct(updatedProduct);
		
		// check product is being updated
		assertEquals(savedProduct.getName(), "Updated name");
	}

	@Test
	public void testDeleteProduct() {
		// create new product
		ProductDto newProduct = new ProductDto();
		newProduct.setName("Duff cola");
		newProduct.setDescription("Super fresh and cold drink.");
		newProduct.setPrice(1.20f);
		newProduct.setCategory(Category.FOOD);
		newProduct.setStock(100);
		
		// save new product
		ProductDto savedProduct = productService.createProduct(newProduct);
		
		// delete saved product
		productService.deleteProduct(savedProduct.getId());
		
		// assert product is being deleted
		assertFalse(productService.existProductById(savedProduct.getId()));
	}
}
