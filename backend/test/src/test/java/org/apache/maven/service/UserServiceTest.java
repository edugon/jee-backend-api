package org.apache.maven.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.apache.maven.app.WebappApplication;
import org.apache.maven.service.dto.UserDto;
import org.apache.maven.types.Role;
import org.apache.maven.types.Salutation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebappApplication.class)
@TestPropertySource(locations="classpath:persistence.properties")
@ContextConfiguration(classes = { UserService.class })
public class UserServiceTest {
	
	@Autowired
	private UserService userService;
	
	private UserDto userDto1;
	private UserDto userDto2;
	
	@Before
	public void setUp() {
		// insert test user
		userDto1 = new UserDto();
		userDto1.setSalutation(Salutation.Mr);
		userDto1.setFirstName("Rick");
		userDto1.setLastName("Sanchez");
		userDto1.setEmail("rick@sanchez");
		userDto1.setUsername("rick");
		userDto1.setPassword("rick");
		userDto1.setRole(Role.ROLE_USER);
		
		if (!userService.existUser(userDto1)) {
			userDto1 = userService.createUser(userDto1);
		} else {
			userDto1 = userService.findUser(userDto1);
		}
		
		// insert test user
		userDto2 = new UserDto();
		userDto2.setSalutation(Salutation.Mr);
		userDto2.setFirstName("Morty");
		userDto2.setLastName("Smith");
		userDto2.setEmail("morty@smith");
		userDto2.setUsername("morty");
		userDto2.setPassword("morty");
		userDto2.setRole(Role.ROLE_USER);
		
		if (!userService.existUser(userDto2)) {
			userDto2 = userService.createUser(userDto2);
		} else {
			userDto2 = userService.findUser(userDto2);
		}
	}

	@After
	public void tearDown() {
		// delete test users
		if (userService.existUserById(userDto1.getId())) {
			userService.deleteUser(userDto1.getId());
		}
		if (userService.existUserById(userDto2.getId())) {
			userService.deleteUser(userDto2.getId());
		}
	}

	@Test
	public void testListUsers() {
		// create user criteria
		UserDto userCriteria = new UserDto();
		userCriteria.setSalutation(Salutation.Mr);
		
		// list billing with Mr salutation
		List<UserDto> userResults = userService.listUsers(0, 10, userCriteria);
		
		// assert getting expected results
		assertEquals(userResults.size(), 2);
	}

	@Test
	public void testFindUser() {
		// create user criteria
		UserDto userCriteria = new UserDto();
		userCriteria.setId(userDto1.getId());
		
		// find first user item
		UserDto foundUser = userService.findUser(userCriteria);
		
		// assert user is being obtained
		assertNotNull(foundUser);
	}
	
	@Test
	public void testCreateUser() {
		// create new user
		UserDto newUser = new UserDto();
		newUser.setSalutation(Salutation.Mr);
		newUser.setFirstName("test");
		newUser.setLastName("test");
		newUser.setEmail("test@test");
		newUser.setUsername("test");
		newUser.setPassword("test");
		newUser.setRole(Role.ROLE_USER);
		
		// save new user
		UserDto savedUser = userService.createUser(newUser);
		
		// assert user is being created
		assertNotNull(savedUser);
		
		// delete saved user
		userService.deleteUser(savedUser.getId());
	}
	
	@Test
	public void testUpdateUser() {
		// create modified user
		UserDto updatedUser = new UserDto();
		updatedUser.setId(userDto1.getId());
		updatedUser.setFirstName("Updated name");
		
		// update user
		UserDto savedUser = userService.updateUser(updatedUser);
		
		// check user is being updated
		assertEquals(savedUser.getFirstName(), "Updated name");
	}
	
	@Test
	public void testDeleteUser() {
		// create new user
		UserDto newUser = new UserDto();
		newUser.setSalutation(Salutation.Mr);
		newUser.setFirstName("test");
		newUser.setLastName("test");
		newUser.setEmail("test@test");
		newUser.setUsername("test");
		newUser.setPassword("test");
		newUser.setRole(Role.ROLE_USER);
		
		// save new user
		UserDto savedUser = userService.createUser(newUser);
		
		// delete saved user
		userService.deleteUser(savedUser.getId());
		
		// assert user is being deleted
		assertFalse(userService.existUserById(savedUser.getId()));
	}
}
