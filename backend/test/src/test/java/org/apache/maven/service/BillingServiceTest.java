package org.apache.maven.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.apache.maven.app.WebappApplication;
import org.apache.maven.service.dto.BillingDto;
import org.apache.maven.service.dto.UserDto;
import org.apache.maven.types.Role;
import org.apache.maven.types.Salutation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebappApplication.class)
@TestPropertySource(locations="classpath:persistence.properties")
@ContextConfiguration(classes = { 
	BillingService.class,
	UserService.class
})
public class BillingServiceTest {
	
	@Autowired
	private BillingService billingService;
	
	@Autowired
	private UserService userService;
	
	private UserDto ownerDto;
	private BillingDto billingDto1;
	private BillingDto billingDto2;
	
	@Before
	public void setUp() {
		// insert test owner
		ownerDto = new UserDto();
		ownerDto.setSalutation(Salutation.Mr);
		ownerDto.setFirstName("Rick");
		ownerDto.setLastName("Sanchez");
		ownerDto.setEmail("rick@sanchez");
		ownerDto.setUsername("rick");
		ownerDto.setPassword("rick");
		ownerDto.setRole(Role.ROLE_USER);
		
		if (!userService.existUser(ownerDto)) {
			ownerDto = userService.createUser(ownerDto);
		} else {
			ownerDto = userService.findUser(ownerDto);
		}
		
		// insert test billing
		billingDto1 = new BillingDto();
		billingDto1.setFirstName("Rick");
		billingDto1.setLastName("Sanchez");
		billingDto1.setCountry("USA");
		billingDto1.setLocality("Washington");
		billingDto1.setAddress("Smith residence");
		billingDto1.setPostalCode(1234);
		billingDto1.setOwner(ownerDto);
		
		if (!billingService.existBilling(billingDto1)) {
			billingDto1 = billingService.createBilling(billingDto1);
		} else {
			billingDto1 = billingService.findBilling(billingDto1);
		}
		
		// insert test billing
		billingDto2 = new BillingDto();
		billingDto2 = new BillingDto();
		billingDto2.setFirstName("Morty");
		billingDto2.setLastName("Smith");
		billingDto2.setCountry("USA");
		billingDto2.setLocality("Washington");
		billingDto2.setAddress("Smith residence");
		billingDto2.setPostalCode(1234);
		billingDto2.setOwner(ownerDto);
		
		if (!billingService.existBilling(billingDto2)) {
			billingDto2 = billingService.createBilling(billingDto2);
		} else {
			billingDto2 = billingService.findBilling(billingDto2);
		}
	}

	@After
	public void tearDown() {
		// delete test billing
		if (billingService.existBilling(billingDto1)) {
			billingService.deleteBilling(billingDto1.getId());
		}
		if (billingService.existBilling(billingDto2)) {
			billingService.deleteBilling(billingDto2.getId());
		}
	}

	@Test
	public void testListBilling() {
		// create billing criteria
		BillingDto billingCriteria = new BillingDto();
		billingCriteria.setCountry("USA");
		
		// list billing with USA country
		List<BillingDto> billingResults = billingService.listBilling(0, 10, billingCriteria);
		
		// assert getting expected results
		assertEquals(billingResults.size(), 2);
	}

	@Test
	public void testFindBilling() {
		// create billing criteria
		BillingDto billingCriteria = new BillingDto();
		billingCriteria.setId(billingDto1.getId());
		
		// find first billing item
		BillingDto foundBilling = billingService.findBilling(billingCriteria);
		
		// assert user is being obtained
		assertNotNull(foundBilling);
	}
	
	@Test
	public void testCreateBilling() {
		// create new billing
		BillingDto newBilling = new BillingDto();
		newBilling = new BillingDto();
		newBilling = new BillingDto();
		newBilling.setFirstName("Jerry");
		newBilling.setLastName("Smith");
		newBilling.setCountry("USA");
		newBilling.setLocality("Washington");
		newBilling.setAddress("Smith residence");
		newBilling.setPostalCode(1234);
		newBilling.setOwner(ownerDto);
		
		// save new billing
		BillingDto savedBilling = billingService.createBilling(newBilling);
		
		// assert user is being created
		assertNotNull(savedBilling);
		
		// delete saved billing
		billingService.deleteBilling(savedBilling.getId());
	}
	
	@Test
	public void testUpdateBilling() {
		// create modified billing
		BillingDto updatedBilling = new BillingDto();
		updatedBilling.setId(billingDto1.getId());
		updatedBilling.setFirstName("Updated name");
		
		// update billing
		BillingDto savedBilling = billingService.updateBilling(updatedBilling);
		
		// check billing is being updated
		assertEquals(savedBilling.getFirstName(), "Updated name");
	}
	
	@Test
	public void testDeleteBilling() {
		// create new billing
		BillingDto newBilling = new BillingDto();
		newBilling = new BillingDto();
		newBilling = new BillingDto();
		newBilling.setFirstName("Jerry");
		newBilling.setLastName("Smith");
		newBilling.setCountry("USA");
		newBilling.setLocality("Washington");
		newBilling.setAddress("Smith residence");
		newBilling.setPostalCode(1234);
		newBilling.setOwner(ownerDto);
		
		// save new billing
		BillingDto savedBilling = billingService.createBilling(newBilling);
		
		// delete first billing
		billingService.deleteBilling(savedBilling.getId());
		
		// assert billing is being deleted
		assertFalse(billingService.existBillingById(savedBilling.getId()));
	}
}
