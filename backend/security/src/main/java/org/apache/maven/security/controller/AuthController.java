package org.apache.maven.security.controller;

import java.io.IOException;
import java.util.logging.Logger;

import org.apache.maven.security.service.AuthService;
import org.apache.maven.service.UserService;
import org.apache.maven.service.dto.UserDto;
import org.apache.maven.service.exception.FieldRequiredException;
import org.apache.maven.types.Role;
import org.apache.maven.utils.constants.ControllerMessages;
import org.apache.maven.utils.constants.ExceptionMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
@RequestMapping("auth")
@Validated
public class AuthController {
	
	private final static Logger LOGGER = Logger.getLogger(AuthController.class.getName());
	
	private final String FIELD_MESSAGE = "message";
	
	private ObjectMapper mapper;
	
	@Autowired
	AuthService authService;
	
	@Autowired
	UserService userService;
	
	/**
	 * Register a new user with role ROLE_USER.
	 * @param newUser User to be created.
	 * @return Success message.
	 * @throws JsonProcessingException Problems processing JSON response.
	 */
	@PostMapping(value = "/signup", consumes = "application/json", produces = "application/json")
	public String signup(@RequestBody(required = true) UserDto newUser) throws JsonProcessingException {
		// create user
		newUser.setRole(Role.ROLE_USER);
		userService.createUser(newUser);
		// create response
		mapper = new ObjectMapper();
		ObjectNode body = mapper.createObjectNode();
		body.put(FIELD_MESSAGE, ControllerMessages.SIGNUP_SUCCESS);
		String bodyString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(body);
		LOGGER.info(ControllerMessages.SIGNUP_SUCCESS);
		return bodyString;
	}
	
	/**
	 * Log in an already existing user.
	 * @param loggedUser User to be logged in.
	 * @return Message with user id and token.
	 * @throws IOException Problems processing JSON response or accessing properties file.
	 */
	@PostMapping(value = "/login", consumes = "application/json", produces = "application/json")
	public UserDto login(@RequestBody(required = true) UserDto loggedUser) throws IOException {
		// check username and password are being sent
		if (loggedUser.getUsername() != null && loggedUser.getPassword() != null) {
			// update user token
			UserDto foundUser = userService.findUser(loggedUser);
			foundUser.setToken(authService.getToken(loggedUser.getUsername()));
			UserDto savedUser = userService.updateUser(foundUser);
			LOGGER.info(ControllerMessages.LOGIN_SUCCESS);
			return savedUser;
		} else {
			throw new FieldRequiredException(ExceptionMessages.USER_REQUIRED);
		}
	}
	
	/**
	 * Log out an already existing user.
	 * We're not block listing token, expiration plus client side removal is enough
	 * @return Success message.
	 * @throws JsonProcessingException Problems processing JSON response.
	 */
	@PostMapping(value = "/logout", consumes = "application/json", produces = "application/json")
	public String logout() throws JsonProcessingException {
		// create response
		mapper = new ObjectMapper();
		ObjectNode body = mapper.createObjectNode();
		body.put(FIELD_MESSAGE, ControllerMessages.LOGOUT_SUCCESS);
		String bodyString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(body);
		return bodyString;
	}
}
