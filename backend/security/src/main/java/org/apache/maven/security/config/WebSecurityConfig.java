package org.apache.maven.security.config;

import org.apache.maven.security.filter.CorsFilter;
import org.apache.maven.security.filter.JWTAuthorizationFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.session.SessionManagementFilter;

/**
 * Spring Security configuration.
 * In charge of adding filters to matched resources depending on authorization.
 * Adds CORS filter to session activity.
 * Adds JWT authorization filter based on username/password format.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		// public access to swagger-ui resources
		web.ignoring().antMatchers("/swagger-ui.html", "/swagger-ui/**", "/v3/api-docs/**");
	}
	
    @Override
	protected void configure(HttpSecurity http) throws Exception {
    	// add CORS and JWT filters
		http.csrf().disable()
			.addFilterBefore(new CorsFilter(), SessionManagementFilter.class)
			.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
			.authorizeRequests()
			.antMatchers("/users").hasRole("ADMIN")
			.antMatchers("/auth/**").permitAll() // public access to auth resources
			.antMatchers(HttpMethod.OPTIONS, "/**").permitAll() // enable preflights
			.anyRequest().authenticated();
	}
}
