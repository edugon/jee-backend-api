package org.apache.maven.security.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.maven.service.UserService;
import org.apache.maven.service.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * Authentication service.
 */
@Service
public class AuthService {
	
	private final String PROPERTY_FILE_NAME = "security.properties";
	private final String PROPERTY_AUTH_HEADER = "auth-header";
	private final String PROPERTY_AUTH_PREFIX = "auth-prefix";
	private final String PROPERTY_AUTH_SECRET = "auth-secret";
	
	@Autowired
	UserService userService;
	
	private final Integer tokenExpiration = 1200000;

	private InputStream inputStream;

	/**
	 * Get properties from security configuration file.
	 * @param propFileName Name of properties file.
	 * @return Properties object obtained from file.
	 * @throws IOException Problems accessing the file.
	 */
	private Properties getSecurityProperties(String propFileName) throws IOException {
		inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
		Properties properties = new Properties();
		if (inputStream != null) {
			properties.load(inputStream);
		} else {
			throw new FileNotFoundException(propFileName + " not found");
		}
		return properties;
	}

	/**
	 * Generate JWT based on user name and secret.
	 * @param username User name that will be associated to token.
	 * @return String with bearer token.
	 * @throws IOException Problems accessing properties file.
	 */
	public String getToken(String username) throws IOException {
		// find user to get role
		UserDto userCriteria = new UserDto();
		userCriteria.setUsername(username);
		UserDto foundUser = userService.findUser(userCriteria);
		// get secret from properties file
		String secretKey = getSecurityProperties(PROPERTY_FILE_NAME).getProperty(PROPERTY_AUTH_SECRET);
		// get authority based on user's role
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList(foundUser.getRole().toString());
		// build token
		String token = Jwts.builder()
				.setId("ecommerceJWT")
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + tokenExpiration))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();
		return "Bearer " + token;
	}

	/**
	 * Validate token from request header.
	 * @param request Request to be validated.
	 * @return Claims obtained from token.
	 * @throws IOException Problems accessing properties file.
	 */
	public Claims validateToken(HttpServletRequest request) throws IOException {
		Properties securityProperties = getSecurityProperties(PROPERTY_FILE_NAME);
		String header = securityProperties.getProperty(PROPERTY_AUTH_HEADER);
		String prefix = securityProperties.getProperty(PROPERTY_AUTH_PREFIX);
		String secret = securityProperties.getProperty(PROPERTY_AUTH_SECRET);
		String jwtToken = request.getHeader(header).replace(prefix, "");
		// we are not validating token against database
		return Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(jwtToken).getBody();
	}

	/**
	 * Add authentication claims to Spring Security context.
	 * @param claims Claims to be added.
	 */
	public void setUpSpringAuthentication(Claims claims) {
		@SuppressWarnings("unchecked")
		List<String> authorities = (List<String>) claims.get("authorities");
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(claims.getSubject(), null,
				authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
		SecurityContextHolder.getContext().setAuthentication(auth);
	}

	/**
	 * Check JWT exists on given request.
	 * @param request Request to be checked
	 * @return boolean indicating whether token exists or not.
	 * @throws IOException Problems accessing properties file.
	 */
	public boolean existToken(HttpServletRequest request) throws IOException {
		Properties securityProperties = getSecurityProperties(PROPERTY_FILE_NAME);
		String header = securityProperties.getProperty(PROPERTY_AUTH_HEADER);
		String prefix = securityProperties.getProperty(PROPERTY_AUTH_PREFIX);
		String authenticationHeader = request.getHeader(header);
		if (authenticationHeader == null || !authenticationHeader.startsWith(prefix))
			return false;
		return true;
	}
}
