package org.apache.maven.types;

public enum Salutation {
	Mr,
	Mrs,
	Ms;
}
