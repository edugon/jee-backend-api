package org.apache.maven.types;

public enum Status {
	PENDING,
	IN_PROGRESS,
	DONE
}
