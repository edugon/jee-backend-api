package org.apache.maven.types;

public enum Role {
	ROLE_ADMIN,
	ROLE_USER;
}
