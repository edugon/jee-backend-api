package org.apache.maven.types;

public enum Category {
	FOOD,
	ELECTRONICS,
	HEALTH;
}
