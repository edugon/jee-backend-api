package org.apache.maven.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@PropertySources({
    @PropertySource("classpath:persistence.properties"),
    @PropertySource("classpath:api.properties"),
	@PropertySource("classpath:security.properties")
})
@ComponentScan(basePackages = {"org.apache.maven"})
@EnableJpaRepositories(basePackages = "org.apache.maven.repository")
@EntityScan(basePackages = "org.apache.maven.entity")
public class WebappApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebappApplication.class, args);
	}

}
