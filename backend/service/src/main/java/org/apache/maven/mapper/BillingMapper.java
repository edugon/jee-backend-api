package org.apache.maven.mapper;

import org.apache.maven.entity.Billing;
import org.apache.maven.service.dto.BillingDto;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", uses = { UserMapper.class })
public interface BillingMapper {
	
	Billing mapDtoToEntity(BillingDto billingDto);
	
	BillingDto mapEntityToDto(Billing billing);
	
	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	void updateModel(BillingDto billingDto, @MappingTarget Billing billing);
	
	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	void updateModel(Billing billing, @MappingTarget BillingDto billingDto);
	
}
