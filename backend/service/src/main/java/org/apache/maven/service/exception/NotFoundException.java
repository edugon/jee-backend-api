package org.apache.maven.service.exception;

public class NotFoundException extends CustomException	{
	
	private static final long serialVersionUID = 1L;

	public NotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
