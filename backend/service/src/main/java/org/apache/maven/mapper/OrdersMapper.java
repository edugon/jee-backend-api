package org.apache.maven.mapper;

import org.apache.maven.entity.Order;
import org.apache.maven.service.dto.OrderDto;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", uses = { UserMapper.class, ProductMapper.class })
public interface OrdersMapper {
	
	Order mapDtoToEntity(OrderDto orderDto);
	
	OrderDto mapEntityToDto(Order order);
	
	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	void updateModel(OrderDto orderDto, @MappingTarget Order order);
	
	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	void updateModel(Order order, @MappingTarget OrderDto orderDto);

}
