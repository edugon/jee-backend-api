package org.apache.maven.service.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.apache.maven.entity.Billing;
import org.apache.maven.mapper.UserMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema
public class BillingDto {

	private Long id;
	
	@NotNull
	private UserDto owner;
	
	@Autowired
	UserMapper userMapper = Mappers.getMapper(UserMapper.class);;
	
	private String firstName;
	private String lastName;
	private String country;
	private String locality;
	private String address;
	private Integer postalCode;
	private Date created;
	private Date updated;
	
	public BillingDto() {
		super();
	}
	
	public BillingDto(Billing billing) {
		this.id = billing.getId();
		this.owner = userMapper.mapEntityToDto(billing.getOwner());
		this.firstName = billing.getFirstName();
		this.lastName = billing.getLastName();
		this.country = billing.getCountry();
		this.locality = billing.getLocality();
		this.address = billing.getAddress();
		this.postalCode = billing.getPostalCode();
		this.created = billing.getCreated();
		this.updated = billing.getUpdated();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserDto getOwner() {
		return owner;
	}

	public void setOwner(UserDto owner) {
		this.owner = owner;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(Integer postalCode) {
		this.postalCode = postalCode;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
}
