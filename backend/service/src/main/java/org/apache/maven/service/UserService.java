package org.apache.maven.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.maven.entity.User;
import org.apache.maven.mapper.UserMapper;
import org.apache.maven.repository.UserRepository;
import org.apache.maven.service.dto.UserDto;
import org.apache.maven.service.exception.AlreadyExistsException;
import org.apache.maven.service.exception.NotFoundException;
import org.apache.maven.utils.constants.ExceptionMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	BillingService billingService;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UserMapper userMapper;
	
	/**
	 * Persist User item.
	 * @param userDto The User item to be persisted.
	 * @return The saved User item.
	 */
	private UserDto saveUser(UserDto userDto) {
		// map dto to entity
		User user = userMapper.mapDtoToEntity(userDto);
		User savedUser = userRepository.save(user);
		return userMapper.mapEntityToDto(savedUser);
	}
	
	/**
	 * List User items based on pagination and filtering.
	 * @param page Value of page index.
	 * @param size Value of page size.
	 * @param userCriteria Filtering criteria parameters.
	 * @return The list of User items.
	 */
	public List<UserDto> listUsers(Integer page, Integer size, UserDto userCriteria) {
		Pageable userPageable = PageRequest.of(page, size, Direction.DESC, "id");
		Page<User> userPage;
		if (userCriteria != null) {
			User user = userMapper.mapDtoToEntity(userCriteria);
			Example<User> userExample = Example.of(user);
			userPage = userRepository.findAll(userExample, userPageable);
		} else {
			userPage = userRepository.findAll(userPageable);
		}
		return StreamSupport.stream(userPage.spliterator(), false)
				.map(UserDto::new)
				.collect(Collectors.toList());
	}
	
	/**
	 * Find User item by given criteria.
	 * @param userCriteria User item to be matched.
	 * @return The specified User item.
	 */
	public UserDto findUser(UserDto userCriteria) {
		User user = userMapper.mapDtoToEntity(userCriteria);
		Example<User> userExample = Example.of(user);
		User foundUser = userRepository.findOne(userExample).orElse(null);
		if (foundUser != null) {
			return userMapper.mapEntityToDto(foundUser);
		} else {
			throw new NotFoundException(ExceptionMessages.USER_NOT_FOUND);
		}
	}
	
	/**
	 * Find User item by specific identifier.
	 * @param userId Identifier of User item.
	 * @return The specified User item.
	 */
	public UserDto findUserById(Long userId) {
		User foundUser = userRepository.findById(userId).orElse(null);
		return userMapper.mapEntityToDto(foundUser);
	}
	
	/**
	 * Create and store new User item.
	 * Users are created with no orders associated.
	 * @param newUser The User item to be stored.
	 * @return The created User item.
	 */
	public UserDto createUser(UserDto newUser) {
		// check user already exists
		if (!existUser(newUser)) {
			return saveUser(newUser);
		} else {
			throw new AlreadyExistsException(ExceptionMessages.USER_ALREADY_EXISTS);
		}
	}
	
	/**
	 * Updated specific User item.
	 * Check User item exists before updating.
	 * @param updatedUser User item to be updated.
	 * @return The updated User item.
	 */
	public UserDto updateUser(UserDto updatedUser) {
		// check user already exists
		if (updatedUser.getId() != null && existUserById(updatedUser.getId())) {
			// update user information
			UserDto foundUser = findUserById(updatedUser.getId());
			User user = userMapper.mapDtoToEntity(foundUser);
			userMapper.updateModel(updatedUser, user);
			updatedUser = userMapper.mapEntityToDto(user);
			return saveUser(updatedUser);
		} else {
			throw new NotFoundException(ExceptionMessages.USER_NOT_FOUND);
		}
	}
	
	/**
	 * Delete specific User item.
	 * Check User item exists before deletion.
	 * @param userId Identifier of User item.
	 */
	public void deleteUser(Long userId) {
		if (existUserById(userId)) {
			userRepository.deleteById(userId);
		} else {
			throw new NotFoundException(ExceptionMessages.USER_NOT_FOUND);
		}
	}
	
	/**
	 * Check User item exists by criteria object.
	 * @param userCriteria User item to be checked
	 * @return boolean indicating whether item exists or not.
	 */
	public boolean existUser(UserDto userCriteria) {
		if (userCriteria != null) {
			User user = userMapper.mapDtoToEntity(userCriteria);
			Example<User> userExample = Example.of(user);
			return userRepository.exists(userExample);
		}
		return false;
	}
	
	/**
	 * Check User item exists by identifier.
	 * @param userId Identifier of User item.
	 * @return boolean indicating whether item exists or not.
	 */
	public boolean existUserById(Long userId) {
		if (userId != null) {
			return userRepository.existsById(userId);
		}
		return false;
	}
}
