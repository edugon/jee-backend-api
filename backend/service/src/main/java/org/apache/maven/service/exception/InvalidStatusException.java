package org.apache.maven.service.exception;

public class InvalidStatusException extends CustomException {
	
	private static final long serialVersionUID = 1L;

	public InvalidStatusException(String errorMessage) {
        super(errorMessage);
    }
}
