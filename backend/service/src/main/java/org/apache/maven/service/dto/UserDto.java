package org.apache.maven.service.dto;

import java.util.Date;

import org.apache.maven.entity.User;
import org.apache.maven.types.Role;
import org.apache.maven.types.Salutation;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema
public class UserDto {

	private Long id;
	private String email;
	private Salutation salutation;
	private String firstName;
	private String lastName;
	private String username;
	private String password;
	private Role role;
	private String token;
	private Date created;
	private Date updated;
	
	public UserDto() {
		super();
	}
	
	public UserDto(User user) {
		this.id = user.getId();
		this.email = user.getEmail();
		this.salutation = user.getSalutation();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.token = user.getToken();
		this.role = user.getRole();
		this.created = user.getCreated();
		this.updated = user.getUpdated();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Salutation getSalutation() {
		return salutation;
	}

	public void setSalutation(Salutation salutation) {
		this.salutation = salutation;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
}
