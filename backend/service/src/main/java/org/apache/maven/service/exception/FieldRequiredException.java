package org.apache.maven.service.exception;

public class FieldRequiredException extends CustomException {
	
	private static final long serialVersionUID = 1L;

	public FieldRequiredException(String errorMessage) {
        super(errorMessage);
    }
}
