package org.apache.maven.service.dto;

import java.util.Date;

import org.apache.maven.entity.Product;
import org.apache.maven.types.Category;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema
public class ProductDto {

	private Long id;
	private String name;
	private String description;
	private Category category;
	private Integer stock;
	private Float price;
	private Date created;
	private Date updated;
	
	public ProductDto() {
		super();
	}
	
	public ProductDto(Product product) {
		this.id = product.getId();
		this.name = product.getName();
		this.description = product.getDescription();
		this.category = product.getCategory();
		this.stock = product.getStock();
		this.price = product.getPrice();
		this.created = product.getCreated();
		this.updated = product.getUpdated();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
}
