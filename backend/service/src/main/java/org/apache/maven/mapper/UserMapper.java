package org.apache.maven.mapper;

import org.apache.maven.entity.User;
import org.apache.maven.service.dto.UserDto;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface UserMapper {
	
	User mapDtoToEntity(UserDto userDto);

	UserDto mapEntityToDto(User user);
	
	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	void updateModel(UserDto userDto, @MappingTarget User user);
	
	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	void updateModel(User user, @MappingTarget UserDto userDto);
	
}
