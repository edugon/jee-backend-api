package org.apache.maven.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.maven.entity.Product;
import org.apache.maven.mapper.ProductMapper;
import org.apache.maven.repository.ProductRepository;
import org.apache.maven.service.dto.ProductDto;
import org.apache.maven.service.exception.AlreadyExistsException;
import org.apache.maven.service.exception.NotFoundException;
import org.apache.maven.utils.constants.ExceptionMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	ProductMapper productMapper;
	
	/**
	 * Persist Product item.
	 * @param productDto Product item to be persiste.
	 * @return The saved Product item.
	 */
	private ProductDto saveProduct(ProductDto productDto) {
		// map dto to entity
		Product product = productMapper.mapDtoToEntity(productDto);
		Product savedProduct = productRepository.save(product);
		return productMapper.mapEntityToDto(savedProduct);
	}
	
	/**
	 * List Product items based on pagination and filtering.
	 * @param page Value of page index.
	 * @param size Value of page size.
	 * @param productCriteria Filtering criteria parameters.
	 * @return The list of Product items.
	 */
	public List<ProductDto> listProducts(Integer page, Integer size, ProductDto productCriteria) {
		Pageable productPageable = PageRequest.of(page, size, Direction.DESC, "id");
		Page<Product> productPage;
		if (productCriteria != null) {
			Product product = productMapper.mapDtoToEntity(productCriteria);
			Example<Product> productExample = Example.of(product);
			productPage = productRepository.findAll(productExample, productPageable);
		} else {
			productPage = productRepository.findAll(productPageable);
		}
		return StreamSupport.stream(productPage.spliterator(), false)
				.map(ProductDto::new)
				.collect(Collectors.toList());
	}
	
	/**
	 * Find Product item by given criteria.
	 * @param productCriteria Product item to be matched.
	 * @return The specified Product item.
	 */
	public ProductDto findProduct(ProductDto productCriteria) {
		Product product = productMapper.mapDtoToEntity(productCriteria);
		Example<Product> productExample = Example.of(product);
		Product foundProduct = productRepository.findOne(productExample).orElse(null);
		if (foundProduct != null) {
			return productMapper.mapEntityToDto(foundProduct);
		} else {
			throw new NotFoundException(ExceptionMessages.PRODUCT_NOT_FOUND);
		}
	}
	
	/**
	 * Find Product item by specific identifier.
	 * @param productId Identifier of Product item.
	 * @return The specified Product item.
	 */
	public ProductDto findProductById(Long productId) {
		Product foundProduct = productRepository.findById(productId).orElse(null);
		return productMapper.mapEntityToDto(foundProduct);
	}
	
	/**
	 * Get a list of Product items by list of identifiers.
	 * @param productIds List of Product ids to be obtained.
	 * @return List of Products items.
	 */
	public List<ProductDto> findProductsById(List<Long> productIds) {
		Iterable<Product> foundProducts = productRepository.findAllById(productIds);
		return StreamSupport.stream(foundProducts.spliterator(), false)
				.map(ProductDto::new)
				.collect(Collectors.toList());
	}
	
	/**
	 * Create and store new Product item.
	 * Products are created with no orders associated.
	 * @param newProduct The Product item to be stored.
	 * @return The created Product item.
	 */
	public ProductDto createProduct(ProductDto newProduct) {
		// check product already exists
		if (!existProduct(newProduct)) {
			return saveProduct(newProduct);
		} else {
			throw new AlreadyExistsException(ExceptionMessages.PRODUCT_ALREADY_EXISTS);
		}
	}
	
	/**
	 * Updated specific Product item.
	 * Check Product item exists before updating.
	 * @param updatedProduct Product item to be updated.
	 * @return The updated Product item.
	 */
	public ProductDto updateProduct(ProductDto updatedProduct) {
		// check product already exists
		if (updatedProduct.getId() != null && existProductById(updatedProduct.getId())) {
			// update product information
			ProductDto foundProduct = findProductById(updatedProduct.getId());
			Product product = productMapper.mapDtoToEntity(foundProduct);
			productMapper.updateModel(updatedProduct, product);
			updatedProduct = productMapper.mapEntityToDto(product);
			return saveProduct(updatedProduct);
		} else {
			throw new NotFoundException(ExceptionMessages.PRODUCT_NOT_FOUND);
		}
	}
	
	/**
	 * Delete specific Product item.
	 * Check Product item exists before deletion.
	 * @param productId Identifier of Product item.
	 */
	public void deleteProduct(Long productId) {
		if (existProductById(productId)) {
			productRepository.deleteById(productId);
		} else {
			throw new NotFoundException(ExceptionMessages.PRODUCT_NOT_FOUND);
		}
	}
	
	/**
	 * Check Product item exists by criteria object.
	 * @param productCriteria Product item to be checked
	 * @return boolean indicating whether item exists or not.
	 */
	public boolean existProduct(ProductDto productCriteria) {
		if (productCriteria.getId() != null) {
			Product product = productMapper.mapDtoToEntity(productCriteria);
			Example<Product> productExample = Example.of(product);
			return productRepository.exists(productExample);
		}
		return false;
	}
	
	/**
	 * Check Product item exists by identifier.
	 * @param productId Identifier of Product item.
	 * @return boolean indicating whether item exists or not.
	 */
	public boolean existProductById(Long productId) {
		if (productId != null) {
			return productRepository.existsById(productId);
		}
		return false;
	}
}
