package org.apache.maven.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.maven.entity.Order;
import org.apache.maven.entity.Product;
import org.apache.maven.entity.User;
import org.apache.maven.mapper.OrdersMapper;
import org.apache.maven.mapper.ProductMapper;
import org.apache.maven.mapper.UserMapper;
import org.apache.maven.repository.OrderRepository;
import org.apache.maven.service.dto.OrderDto;
import org.apache.maven.service.dto.ProductDto;
import org.apache.maven.service.dto.UserDto;
import org.apache.maven.service.exception.AlreadyExistsException;
import org.apache.maven.service.exception.InvalidStatusException;
import org.apache.maven.service.exception.NotFoundException;
import org.apache.maven.types.Status;
import org.apache.maven.utils.constants.ExceptionMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
	
	@Autowired
	UserService userService;
	
	@Autowired
	ProductService productService;

	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	OrdersMapper orderMapper;
	
	@Autowired
	UserMapper userMapper;
	
	@Autowired
	ProductMapper productMapper;
	
	/**
	 * List Order items based on pagination and filtering.
	 * @param page Value of page index.
	 * @param size Value of page size.
	 * @param orderCriteria Filtering criteria parameters.
	 * @return The list of Order items.
	 */
	public List<OrderDto> listOrders(Integer page, Integer size, OrderDto orderCriteria) {
		Pageable orderPageable = PageRequest.of(page, size, Direction.DESC, "id");
		Page<Order> orderPage;
		if (orderCriteria != null) {
			Order order = orderMapper.mapDtoToEntity(orderCriteria);
			Example<Order> orderExample = Example.of(order);
			orderPage = orderRepository.findAll(orderExample, orderPageable);
		} else {
			orderPage = orderRepository.findAll(orderPageable);
		}
		return StreamSupport.stream(orderPage.spliterator(), false)
				.map(OrderDto::new)
				.collect(Collectors.toList());
	}
	
	/**
	 * Find Order item by given criteria.
	 * @param orderCriteria Order item to be matched.
	 * @return The specified Order item.
	 */
	public OrderDto findOrder(OrderDto orderCriteria) {
		Order order = orderMapper.mapDtoToEntity(orderCriteria);
		Example<Order> orderExample = Example.of(order);
		Order foundOrder = orderRepository.findOne(orderExample).orElse(null);
		if (foundOrder != null) {
			return orderMapper.mapEntityToDto(foundOrder);
		} else {
			throw new NotFoundException(ExceptionMessages.ORDER_NOT_FOUND);
		}
	}
	
	/**
	 * Find Order item by specific identifier.
	 * @param orderId Identifier of Order item.
	 * @return The specified Order item.
	 */
	public OrderDto findOrderById(Long orderId) {
		Order foundOrder = orderRepository.findById(orderId).orElse(null);
		if (foundOrder != null) {
			return orderMapper.mapEntityToDto(foundOrder);
		} else {
			throw new NotFoundException(ExceptionMessages.ORDER_NOT_FOUND);
		}
	}
	
	/**
	 * Create and store new Order item.
	 * Check Order item exists before creation.
	 * Restriction: New orders must have PENDING status.
	 * @param newOrder The Order item to be stored.
	 * @return The created Order item.
	 */
	public OrderDto createOrder(OrderDto newOrder) {
		// check order status
		if (newOrder.getStatus() == Status.PENDING) {
			// check order already exists
			if(!existOrder(newOrder)) {
				// validate owner and products
				List<ProductDto> productDtos = newOrder.getProducts();
				// check owner exists
				if (newOrder.getOwner() != null && userService.existUserById(newOrder.getOwner().getId())) {
					// check products exist
					if (productDtos.size() > 0 && productDtos.stream().allMatch((p) -> productService.existProductById(p.getId()))) {
						// update owner information
						// we need to find owner and products (possibility of partial data)
						UserDto foundOwner = userService.findUserById(newOrder.getOwner().getId());
						User owner = userMapper.mapDtoToEntity(foundOwner);
						// update products information
						List<Product> products = new ArrayList<Product>();
						productDtos.forEach((productDto) -> {
							Product foundProduct = productMapper.mapDtoToEntity(productService.findProductById(productDto.getId()));
							products.add(foundProduct);
						});
						// save order
						Order order = orderMapper.mapDtoToEntity(newOrder);
						order.setOwner(owner);
						order.setProducts(products);
						Order savedOrder = orderRepository.save(order);
						return orderMapper.mapEntityToDto(savedOrder);
					} else {
						throw new NotFoundException(ExceptionMessages.PRODUCT_NOT_FOUND);
					}
				} else {
					throw new NotFoundException(ExceptionMessages.OWNER_NOT_FOUND);
				}
			} else {
				throw new AlreadyExistsException(ExceptionMessages.ORDER_ALREADY_EXISTS);
			}
		} else {
			throw new InvalidStatusException(ExceptionMessages.PRODUCT_INVALID_STATUS);
		}
	}
	
	/**
	 * Updated specific Order item.
	 * Check Order item exists before updating.
	 * @param updatedOrder Order item to be updated.
	 * @return The updated Order item.
	 */
	public OrderDto updateOrder(OrderDto updatedOrder) {
		// check order already exists
		if(updatedOrder.getId() != null && existOrderById(updatedOrder.getId())) {
			// update order information
			OrderDto foundOrder = findOrderById(updatedOrder.getId());
			Order order = orderMapper.mapDtoToEntity(foundOrder);
			// update owner if exist
			if (updatedOrder.getOwner() != null && userService.existUserById(updatedOrder.getOwner().getId())) {
				userMapper.updateModel(updatedOrder.getOwner(), order.getOwner());
			}
			// update products if exist
			Boolean productsExist = updatedOrder.getProducts()
					.stream().allMatch((p) -> productService.existProductById(p.getId()));
			if (updatedOrder.getProducts() != null && productsExist) {
				productMapper.updateList(updatedOrder.getProducts(), order.getProducts());
			}
			// update order
			orderMapper.updateModel(updatedOrder, order);
			Order savedOrder = orderRepository.save(order);
			return orderMapper.mapEntityToDto(savedOrder);
		} else {
			throw new NotFoundException(ExceptionMessages.ORDER_NOT_FOUND);
		}
	}
	
	/**
	 * Delete specific Order item.
	 * Check Order item exists before deletion.
	 * @param billingId Identifier of Order item.
	 */
	public void deleteOrder(Long orderId) {
		if (existOrderById(orderId)) {
			orderRepository.deleteById(orderId);
		} else {
			throw new NotFoundException(ExceptionMessages.ORDER_NOT_FOUND);
		}
	}
	
	/**
	 * Check Order item exists by criteria object.
	 * @param orderCriteria Order item to be checked
	 * @return boolean indicating whether item exists or not.
	 */
	public boolean existOrder(OrderDto orderCriteria) {
		if (orderCriteria.getId() != null) {
			Order order = orderMapper.mapDtoToEntity(orderCriteria);
			Example<Order> orderExample = Example.of(order);
			return orderRepository.exists(orderExample);
		}
		return false;
	}
	
	/**
	 * Check Order item exists by identifier.
	 * @param orderId Identifier of Order item.
	 * @return boolean indicating whether item exists or not.
	 */
	public boolean existOrderById(Long orderId) {
		if (orderId != null) {
			return orderRepository.existsById(orderId);
		}
		return false;
	}
}
