package org.apache.maven.service.dto;

import java.util.Date;
import java.util.List;

import org.apache.maven.entity.Order;
import org.apache.maven.mapper.BillingMapper;
import org.apache.maven.mapper.ProductMapper;
import org.apache.maven.mapper.UserMapper;
import org.apache.maven.types.Status;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema
public class OrderDto {

	private Long id;
	private UserDto owner;
	private BillingDto billing;
	private List<ProductDto> products;
	private Status status;
	private Date created;
	private Date updated;
	
	@Autowired
	ProductMapper productMapper = Mappers.getMapper(ProductMapper.class);
	
	@Autowired
	UserMapper userMapper = Mappers.getMapper(UserMapper.class);
	
	@Autowired
	BillingMapper billingMapper = Mappers.getMapper(BillingMapper.class);
	
	public OrderDto() {
		super();
	}
	
	public OrderDto(Order order) {
		this.id = order.getId();
		this.owner = userMapper.mapEntityToDto(order.getOwner());
		this.billing = billingMapper.mapEntityToDto(order.getBilling());
		this.products = productMapper.mapEntityList((order.getProducts()));
		this.status = order.getStatus();
		this.created = order.getCreated();
		this.updated = order.getUpdated();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserDto getOwner() {
		return owner;
	}

	public void setOwner(UserDto owner) {
		this.owner = owner;
	}

	public BillingDto getBilling() {
		return billing;
	}

	public void setBilling(BillingDto billing) {
		this.billing = billing;
	}

	public List<ProductDto> getProducts() {
		return products;
	}

	public void setProducts(List<ProductDto> products) {
		this.products = products;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
}
