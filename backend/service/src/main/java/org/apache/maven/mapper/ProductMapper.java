package org.apache.maven.mapper;

import java.util.Collection;
import java.util.List;

import org.apache.maven.entity.Product;
import org.apache.maven.service.dto.ProductDto;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface ProductMapper {
	
	Product mapDtoToEntity(ProductDto productDto);
	
	ProductDto mapEntityToDto(Product product);
	
	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	void updateModel(ProductDto productDto, @MappingTarget Product product);
	
	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	void updateModel(Product product, @MappingTarget ProductDto productDto);
	
	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	void updateList(List<Product> products, @MappingTarget List<ProductDto> productDtos);
	
	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	void updateList(List<ProductDto> productDtos, @MappingTarget Collection<Product> products);
	
	List<ProductDto> mapEntityList(Collection<Product> products);
	
	Collection<Product> mapDtoList(List<ProductDto> productsDto);
	
}
