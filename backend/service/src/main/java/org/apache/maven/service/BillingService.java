package org.apache.maven.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.maven.entity.Billing;
import org.apache.maven.entity.User;
import org.apache.maven.mapper.BillingMapper;
import org.apache.maven.mapper.UserMapper;
import org.apache.maven.repository.BillingRepository;
import org.apache.maven.service.dto.BillingDto;
import org.apache.maven.service.dto.UserDto;
import org.apache.maven.service.exception.AlreadyExistsException;
import org.apache.maven.service.exception.NotFoundException;
import org.apache.maven.utils.constants.ExceptionMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
public class BillingService {
	
	@Autowired
	UserService userService;

	@Autowired
	BillingRepository billingRepository;
	
	@Autowired
	BillingMapper billingMapper;
	
	@Autowired
	UserMapper userMapper;
	
	/**
	 * Persist Billing item.
	 * Validate and obtain owner information.
	 * @param billingDto Billing item to be persisted.
	 * @return The saved Billing item.
	 */
	private BillingDto saveBilling(BillingDto billingDto) {
		// check owner exists
		if (userService.existUserById(billingDto.getOwner().getId())) {
			// complete billing information
			UserDto foundOwner = userService.findUserById(billingDto.getOwner().getId());
			// map dto to entity
			Billing billing = billingMapper.mapDtoToEntity(billingDto);
			User billingUser = userMapper.mapDtoToEntity(foundOwner);
			billing.setOwner(billingUser);
			Billing savedBilling = billingRepository.save(billing);
			return billingMapper.mapEntityToDto(savedBilling);
		} else {
			throw new NotFoundException(ExceptionMessages.OWNER_NOT_FOUND);
		}
	}
	
	/**
	 * List Billing items based on pagination and filtering.
	 * @param page Value of page index.
	 * @param size Value of page size.
	 * @param billingCriteria Filtering criteria parameters.
	 * @return The list of Billing items.
	 */
	public List<BillingDto> listBilling(Integer page, Integer size, BillingDto billingCriteria) {
		Pageable billingPageable = PageRequest.of(page, size, Direction.DESC, "id");
		Page<Billing> billingPage;
		if (billingCriteria != null) {
			Billing billing = billingMapper.mapDtoToEntity(billingCriteria);
			Example<Billing> billingExample = Example.of(billing);
			billingPage = billingRepository.findAll(billingExample, billingPageable);
		} else {
			billingPage = billingRepository.findAll(billingPageable);
		}
		return StreamSupport.stream(billingPage.spliterator(), false)
				.map(BillingDto::new)
				.collect(Collectors.toList());
	}
	
	/**
	 * Find Billing item by given criteria.
	 * @param billingCriteria Billing item to be matched.
	 * @return The specified Billing item.
	 */
	public BillingDto findBilling(BillingDto billingCriteria) {
		Billing billing = billingMapper.mapDtoToEntity(billingCriteria);
		Example<Billing> billingExample = Example.of(billing);
		Billing foundBilling = billingRepository.findOne(billingExample).orElse(null);
		if (foundBilling != null) {
			return billingMapper.mapEntityToDto(foundBilling);
		} else {
			throw new NotFoundException(ExceptionMessages.BILLING_NOT_FOUND);
		}
	}
	
	/**
	 * Find Billing item by specific identifier.
	 * @param billingId Identifier of Billing item.
	 * @return The specified Billing item.
	 */
	public BillingDto findBillingById(Long billingId) {
		Billing foundBilling = billingRepository.findById(billingId).orElse(null);
		return billingMapper.mapEntityToDto(foundBilling);
	}
	
	/**
	 * Create and store new Billing item.
	 * Check billing item exists before creation.
	 * @param newBilling The Billing item to be stored.
	 * @return The created Billing item.
	 */
	public BillingDto createBilling(BillingDto newBilling) {
		// check billing already exists
		if(!existBilling(newBilling)) {
			return saveBilling(newBilling);
		} else {
			throw new AlreadyExistsException(ExceptionMessages.BILLING_ALREADY_EXISTS);
		}
	}
	
	/**
	 * Updated specific Billing item.
	 * Check Billing item exists before updating.
	 * @param updatedBilling Billing item to be updated.
	 * @return The updated Billing item.
	 */
	public BillingDto updateBilling(BillingDto updatedBilling) {
		// check billing already exists
		if (updatedBilling.getId() != null && existBillingById(updatedBilling.getId())) {
			// update billing information
			BillingDto foundBilling = findBillingById(updatedBilling.getId());
			Billing billing = billingMapper.mapDtoToEntity(foundBilling);
			billingMapper.updateModel(updatedBilling, billing);
			updatedBilling = billingMapper.mapEntityToDto(billing);
			return saveBilling(updatedBilling);
		} else {
			throw new NotFoundException(ExceptionMessages.BILLING_NOT_FOUND);
		}
	}
	
	/**
	 * Delete specific Billing item.
	 * Check Billing item exists before deletion.
	 * @param billingId Identifier of Billing item.
	 */
	public void deleteBilling(Long billingId) {
		if (existBillingById(billingId)) {
			billingRepository.deleteById(billingId);
		} else {
			throw new NotFoundException(ExceptionMessages.BILLING_NOT_FOUND);
		}
	}
	
	/**
	 * Check Billing item exists by criteria object.
	 * @param billingCriteria Billing item to be checked
	 * @return boolean indicating whether item exists or not.
	 */
	public boolean existBilling(BillingDto billingCriteria) {
		if (billingCriteria != null) {
			Billing billing = billingMapper.mapDtoToEntity(billingCriteria);
			Example<Billing> billingExample = Example.of(billing);
			return billingRepository.exists(billingExample);
		}
		return false;
	}
	
	/**
	 * Check Billing item exists by identifier.
	 * @param billingId Identifier of Billing item.
	 * @return boolean indicating whether item exists or not.
	 */
	public boolean existBillingById(Long billingId) {
		if (billingId != null) {
			return billingRepository.existsById(billingId);
		}
		return false;
	}
}
