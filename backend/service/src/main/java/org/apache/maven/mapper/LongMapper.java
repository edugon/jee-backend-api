package org.apache.maven.mapper;

import org.apache.maven.entity.Product;
import org.apache.maven.entity.User;
import org.apache.maven.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LongMapper {
	
	@Autowired
	ProductRepository productRepository;

	public Long map(User user) {
		return user.getId();
	}
	
	public Long map(Product product) {
		return product.getId();
	}
	
	public Product map(Long id) {
		return productRepository.findById(id).orElse(null);
	}
}
