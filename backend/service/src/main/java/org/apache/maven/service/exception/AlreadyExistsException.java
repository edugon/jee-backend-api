package org.apache.maven.service.exception;

public class AlreadyExistsException extends CustomException	{
	
	private static final long serialVersionUID = 1L;

	public AlreadyExistsException(String errorMessage) {
        super(errorMessage);
    }
}
