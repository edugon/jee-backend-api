package org.apache.maven.utils.constants;

public class ControllerMessages {
	
	public static final String SIGNUP_SUCCESS = "Sign up succeeded!";
	public static final String LOGIN_SUCCESS = "Log in succeeded!";
	public static final String LOGOUT_SUCCESS = "Log out succeeded!";
	
}
