package org.apache.maven.utils.constants;

public class ExceptionMessages {
	
	public static final String BILLING_ALREADY_EXISTS = "Billing already exists!";
	public static final String BILLING_NOT_FOUND = "Billing not found!";
	
	public static final String ORDER_NOT_FOUND = "Order not found!";
	public static final String ORDER_ALREADY_EXISTS = "Order already exists!";
	
	public static final String OWNER_NOT_FOUND = "Owner not found!";
	public static final String USER_ALREADY_EXISTS = "User already exists!";
	public static final String USER_NOT_FOUND = "User not found!";
	public static final String USER_REQUIRED = "Username and password are required";
	
	public static final String PRODUCT_NOT_FOUND = "Products not found!";
	public static final String PRODUCT_INVALID_STATUS = "New orders should have PENDING status!";
	public static final String PRODUCT_ALREADY_EXISTS = "Product already exists!";
	
}
