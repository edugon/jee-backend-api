-- Create database schema
create table billings (id bigint not null auto_increment, address varchar(255) not null, country varchar(255) not null, created datetime(6) not null, first_name varchar(255) not null, last_name varchar(255) not null, locality varchar(255) not null, postal_code integer not null, updated datetime(6), user bigint not null, primary key (id)) engine=InnoDB;
create table orders (id bigint not null auto_increment, created datetime(6) not null, status varchar(255) not null, updated datetime(6), user bigint not null, billing bigint, primary key (id)) engine=InnoDB;
create table orders_products (order_id bigint not null, product_id bigint not null) engine=InnoDB;
create table products (id bigint not null auto_increment, category varchar(255) not null, created datetime(6) not null, description varchar(255), name varchar(255) not null, price float not null, stock integer not null, updated datetime(6), primary key (id)) engine=InnoDB;
create table users (id bigint not null auto_increment, created datetime(6) not null, email varchar(255) not null, first_name varchar(255), last_name varchar(255), password varchar(30) not null, role varchar(255) not null, salutation varchar(255) not null, token varchar(255), updated datetime(6), username varchar(15) not null, primary key (id)) engine=InnoDB;
alter table products add constraint UK_o61fmio5yukmmiqgnxf8pnavn unique (name);
alter table users add constraint UK_6dotkott2kjsp8vw4d0m25fb7 unique (email);
alter table users add constraint UK_r43af9ap4edm43mmtq01oddj6 unique (username);
alter table billings add constraint FKin8e43i0gs6kn20i5cbedy0qc foreign key (user) references users (id);
alter table orders add constraint FKbcyw1rnwiup2kx6tfg4408anm foreign key (user) references users (id);
alter table orders add constraint FKin8e43i0gs6kn20i5asd12ert foreign key (billing) references billings (id);
alter table orders_products add constraint FK43vke5jd6eyasd92t3k24kdxq foreign key (product_id) references products (id);
alter table orders_products add constraint FKe4y1sseio787e4o5hrml7omt5 foreign key (order_id) references orders (id);

-- Insert admin user
INSERT INTO ecommerce.users (created,email,first_name,last_name,password,`role`,salutation,token,updated,username) VALUES 
(NOW(),'batman@batman','Bruce','Wayne','batman','ROLE_ADMIN','Mr',NULL,NULL,'batman');

-- Insert normal users
INSERT INTO ecommerce.users (created,email,first_name,last_name,password,`role`,salutation,token,updated,username) VALUES 
(NOW(),'superman@superman','Clark','Kent','superman','ROLE_USER','Mr',NULL,NULL,'superman');
INSERT INTO ecommerce.users (created,email,first_name,last_name,password,`role`,salutation,token,updated,username) VALUES 
(NOW(),'spiderman@spiderman','Peter','Parker','spiderman','ROLE_USER','Mr',NULL,NULL,'spiderman');

-- Insert some products
INSERT INTO ecommerce.products (category,created,description,name,price,stock,updated) VALUES 
('FOOD',NOW(),'Super fresh and cold drink.','Coca-cola',1.20,200,NULL);
INSERT INTO ecommerce.products (category,created,description,name,price,stock,updated) VALUES 
('FOOD',NOW(),'Fresh and cold drink but Atomic!','Nuka-cola',1.20,0,NULL);
INSERT INTO ecommerce.products (category,created,description,name,price,stock,updated) VALUES 
('ELECTRONICS',NOW(),'Mobile phone that comes with Android.','Samsung S2000',99.99,50,NULL);
INSERT INTO ecommerce.products (category,created,description,name,price,stock,updated) VALUES 
('HEALTH',NOW(),'Wash your hair like never before','Shampoo v9000',5.20,400,NULL);
INSERT INTO ecommerce.products (category,created,description,name,price,stock,updated) VALUES 
('ELECTRONICS',NOW(),'Very useful for coding!','Laptop Alienware Gaming',599.99,10,NULL);
INSERT INTO ecommerce.products (category,created,description,name,price,stock,updated) VALUES 
('FOOD',NOW(),'This drink gives you wings!','Redbull',1.20,400,NULL);
INSERT INTO ecommerce.products (category,created,description,name,price,stock,updated) VALUES 
('HEALTH',NOW(),'Do not smell bad','Super deodorant',2.00,500,NULL);
INSERT INTO ecommerce.products (category,created,description,name,price,stock,updated) VALUES 
('HEALTH',NOW(),'Now electric! Useful huh?','Toothbrush deluxe',1.20,400,NULL);
